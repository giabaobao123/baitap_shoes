import React, { useState } from 'react'
import Modal from './Modal'
import ProductList from './ProductList'
import data from '../DataJson/shoesData.json'

const BaiTapShoes = () => {
  const [state,setState] = useState(data[0])
  let xemChiTiet  = (sp) => {
    setState(sp)
  }
    return (
        <div className='container'>
            <h3 className='text-center text-danger'>Bài Tập shoes</h3>
        <Modal sp = {state}/>
      <div className='row'>
      <ProductList data={data}  xemChiTiet={xemChiTiet}/> 
      </div>
    </div>
  )
}

export default BaiTapShoes