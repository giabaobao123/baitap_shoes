import React from "react";

const Modal = (props) => {
    console.log(props.sp.id)

  return (
    <div>
      <div
        class="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog" style={{maxWidth:'1000px'}} role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                Xem Chi Tiết Sản Phẩm
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <table className="table">
                    <thead>
                        <th>Mã SP</th>
                        <th>Tên SP</th>  
                        <th>Hình Ảnh</th>
                        <th>Giá SP</th>
                        <th>Số lượng</th>
                        <th>Mô Tả</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{props.sp.id}</td>
                            <td>{props.sp.name}</td>
                            <td><img src={props.sp.image} alt="" width={150} height={200}/></td>
                            <td>{props.sp.price}</td>
                            <td>{props.sp.quanlity}</td>
                            <td>{props.sp.shortDescription}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
