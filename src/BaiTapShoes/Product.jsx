import React from "react";

const Product = (props) => {
  return (
    <div className="card text-left">
      <img className="card-img-top" src={props.sp.image} width={200} height={250} alt />
      <div className="card-body">
        <h5 className="card-title">{props.sp.name}</h5>
        <p className="card-text">{props.sp.price}$</p>
        <div>
            <button className="btn btn-dark" data-toggle="modal" data-target="#exampleModal" onClick={()=> props.xemChiTiet(props.sp)}>Xem Chi Tiết</button>
        </div>
      </div>
    </div>
  );
};

export default Product;
