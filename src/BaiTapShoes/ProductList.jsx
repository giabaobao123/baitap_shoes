import React from 'react'
import Product from './Product'

const ProductList = (props) => {
  return (
     props.data.map((sp) => {
        return (
           <div className='col-4'>
             <Product sp = {sp} xemChiTiet = {props.xemChiTiet}/>
           </div>
        )
     })
  )
}

export default ProductList